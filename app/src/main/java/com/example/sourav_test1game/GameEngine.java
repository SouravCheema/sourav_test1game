package com.example.sourav_test1game;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.graphics.Rect;
import android.support.constraint.solver.widgets.Rectangle;

import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    // Android debug variables
    final static String TAG="DisgustingGame";

    // screen size
    int screenHeight;
    int screenWidth;

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;



    // -----------------------------------
    // GAME SPECIFIC VARIABLES
    // -----------------------------------

    // ----------------------------
    // ## SPRITES
    // ----------------------------
    Nose nose;
    Finger finger;

    // ----------------------------
    // ## GAME STATS
    // ----------------------------
    int score = 0;

    public GameEngine(Context context, int w, int h) {
        super(context);


        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;


        this.printScreenInfo();


        this.spawnFinger();
        this.spawnNose();


    }


    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }

    private void spawnNose() {

        nose = new Nose(this.getContext(), 100, 100);

    }
    private void spawnFinger() {
        Random random = new Random();


        finger = new Finger(this.getContext(), 100, (this.screenHeight-300) / 2);
    }

    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------
    boolean nosePicked;
    public void updatePositions() {

        finger.updateFingerPosition();

        if (finger.getXPosition() <= 0) {
        finger.setXPosition(this.screenWidth);
        }

        if (finger.getHitbox().intersect(nose.getHitboxleft()) || finger.getHitbox().intersect(nose.getHitboxRight())) {

            // reduce lives
            score++;
            System.out.println("You Win");

        }
        else{
            System.out.println("You Lose");
        }


    }

    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------


            this.canvas.drawColor(Color.argb(255,255,255,255));
            paintbrush.setColor(Color.WHITE);



            canvas.drawBitmap(this.finger.getBitmap(), this.finger.getXPosition(), this.finger.getYPosition(), paintbrush);


            canvas.drawBitmap(this.nose.getBitmap(), this.nose.getXPosition(), this.nose.getYPosition(), paintbrush);

            paintbrush.setColor(Color.BLUE);
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(5);
            //canvas.drawRect(510, 410, 60, 220, paintbrush);
            Rect fingerHitbox = finger.getHitbox();
            Rect NoseHitboxleft = nose.getHitboxleft();
            Rect NoseHitboxRight = nose.getHitboxRight();
            canvas.drawRect(fingerHitbox.left, fingerHitbox.top, fingerHitbox.right, fingerHitbox.bottom, paintbrush);

            canvas.drawRect(NoseHitboxleft.left, NoseHitboxleft.top, NoseHitboxleft.right, NoseHitboxleft.bottom, paintbrush);
            canvas.drawRect(NoseHitboxRight.left, NoseHitboxRight.top, NoseHitboxRight.right, NoseHitboxRight.bottom, paintbrush);
            this.holder.unlockCanvasAndPost(canvas);

            paintbrush.setTextSize(12);
            paintbrush.setColor(Color.GREEN);
            canvas.drawText("Nose Picked "+ score + "times ", 300, 400, paintbrush);
            canvas.drawText("Nose Missed" + score + "times",300,400,paintbrush);
        }
    }

    public void setFPS() {
        try {
            gameThread.sleep(100);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_DOWN) {

            this.finger.setDirection(1);
        }
        else if (userAction == MotionEvent.ACTION_UP) {

            this.finger.setDirection(0);
        }

        return true;
    }
}
