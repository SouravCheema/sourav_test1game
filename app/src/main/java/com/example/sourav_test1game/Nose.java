package com.example.sourav_test1game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class Nose {
    int xPosition;
    int yPosition;
    int direction;
    Bitmap image;
    private Rect hitBoxleft;
    private Rect hitBoxright;

    public Nose(Context context, int x, int y) {
        this.image = BitmapFactory.decodeResource(context.getResources(), R.drawable.nose01);
        this.xPosition = x;
        this.yPosition = y;
        this.hitBoxleft = new Rect(this.xPosition, this.yPosition, this.xPosition + this.image.getWidth(), this.yPosition + this.image.getHeight());
        this.hitBoxright = new Rect(this.xPosition, this.yPosition, this.xPosition + this.image.getWidth(), this.yPosition + this.image.getHeight());
    }

    public void setXPosition(int x) {
        this.xPosition = x;
    }
    public void setYPosition(int y) {
        this.yPosition = y;
    }
    public int getXPosition() {
        return this.xPosition;
    }
    public int getYPosition() {
        return this.yPosition;
    }

    public Rect getHitboxleft() {
        return this.hitBoxleft;
    }
    public Rect getHitboxRight() {
        return this.hitBoxright;
    }




    public Bitmap getBitmap() {
        return this.image;
    }

}
