package com.example.sourav_test1game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;


public class Finger  {
    private final Rect hitBox;

    int xPosition;
    int yPosition;
    int direction = -1;
    Bitmap playerImage;


    public Finger(Context context, int x, int y) {
        this.playerImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.finger01);
        this.xPosition = x;
        this.yPosition = y;

        this.hitBox = new Rect(this.xPosition, this.yPosition, this.xPosition + this.playerImage.getWidth(), this.yPosition + this.playerImage.getHeight());

    }


    public void updateFingerPosition() {
                if (this.direction == 0) {
                        // move down
                                this.xPosition = this.xPosition + 15;
                                this.yPosition = this.yPosition + 25;
                            }
                else if (this.direction == 1) {
                        // move up
                                this.xPosition = this.xPosition + 15;
//                                this.yPosition = this.yPosition + 15;
                   }
            this.hitBox.top = this.yPosition;
               this.hitBox.bottom = this.yPosition + this.playerImage.getHeight();
               this.updateHitbox();
            }

    public Rect getHitbox() {
                return this.hitBox;
            }

    public void updateHitbox() {
                // update the position of the hitbox
                        this.hitBox.top = this.yPosition;
               this.hitBox.left = this.xPosition;
                this.hitBox.right = this.xPosition + this.playerImage.getWidth();
                this.hitBox.bottom = this.yPosition + this.playerImage.getHeight();
    }

    public void setXPosition(int x) {
        this.xPosition = x;
    }
    public void setYPosition(int y) {
        this.yPosition = y;
    }


    public int getXPosition() {
        return this.xPosition;
    }
    public int getYPosition() {
        return this.yPosition;
    }

    public void setDirection(int i){
        this.direction = i;
    }

    public Bitmap getBitmap() {
        return this.playerImage;
    }

}
